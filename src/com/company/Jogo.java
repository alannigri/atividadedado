package com.company;

public class Jogo {
    private int repeticoes, soma;
    private int[] numeros = new int[4];
    Sorteio sorteio = new Sorteio();
    Dado dado = new Dado();

    public Jogo(int lados, int repeticoes) {
        this.repeticoes = repeticoes;
        dado.setLados(lados);
    }

    public void jogar() {
        for (int i = 0; i < repeticoes; i++) {
            soma = 0;
            for (int y = 0; y < numeros.length-1; y++) {
                numeros[y] = sorteio.sortear(dado.getLados());
                soma += numeros[y];
            }
            numeros[numeros.length - 1] = soma;
            IO.resultado(numeros);
        }
    }
}
