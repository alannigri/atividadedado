package com.company;

import java.util.Scanner;

public class IO {
    Scanner scanner = new Scanner(System.in);

    public int getLados() {
        System.out.println("Digite quantos lados seu dado possui: ");
        int lados = scanner.nextInt();
        return lados;
    }

    public int getRepeticoes() {
        System.out.println("Digite quantas repetiçoes vc quer jogar: ");
        int repeticoes = scanner.nextInt();
        return repeticoes;
    }

    public static void resultado(int num[]) {
        int i = 1;
        for (int num1 : num) {
            if (num.length == i) {
                System.out.print(num1);
                System.out.println();
            } else {
                System.out.print(num1 + ",");
            }
            i++;
        }
    }

    public static void resultado(int num1, int num2, int num3, int soma) {
        System.out.println(num1 + "," + num2 + "," + num3 + "," + soma);
    }
}
