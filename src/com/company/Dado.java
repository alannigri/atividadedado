package com.company;

public class Dado {
    private int lados;

    public Dado() {
    }

    public Dado(int lados) {
        this.lados = lados;
    }

    public void setLados(int lados) {
        this.lados = lados;
    }

    public int getLados() {
        return lados;
    }
}


