package com.company;

public class Main {

    public static void main(String[] args) {
        IO io = new IO();
        Jogo jogo = new Jogo(io.getLados(), io.getRepeticoes());
        Jogador jogador = new Jogador();
        jogador.jogar(jogo);

    }
}
